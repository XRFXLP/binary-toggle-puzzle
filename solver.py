from functools import reduce
from itertools import product
def blend(a, b):
    r = []
    for i in a:
        for j in b:
            if all(p == q for p, q in zip(i, j) if -1 not in [p, q]):
                r.append(tuple([p if p == q else max(p, q) for p, q in zip(i, j)]))
            if r:
                return r
    return r

def brute_force(mat):
    cn = []
    for row in mat[-2:]:
        a = [[0, 1] if i else [-1] for i in row[:-1]]
        cn.append([tuple(i) for i in product(*a) if i.count(1) % 2 == row[-1]])
    if not cn:
        return []
    sols = reduce(blend, cn[::-1])
    for row in mat[:-2][::-1]:
        a = [[0, 1] if i and sols[-1][j] == -1 else [max(sols[-1][j], -1)] for j, i in enumerate(row[:-1])]
        # print(a)
        # print(tuple(row[:-1]))
        # print(sols[-1])
        sols = blend(sols, [tuple(i) for i in product(*a) if sum(k * l for k, l in zip(i, row) if k != -1 and l == 1) % 2 == row[-1]])
    #print([sum(a*b for a, b in zip(i, sols[0])) % 2 == i[-1] for i in mat])
    return sols[0]



def solve(Q):
    Con = []
    for i in range(len(Q)):
        for j in range(len(Q)):
            V = [(i, k) for k in range(len(Q))]
            V += [(k, j) for k in range(len(Q)) if k != i]
            C = [int(a == i or b == j) for a in range(len(Q)) for b in range(len(Q))] + [1 - Q[i][j]]
            Con.append(C)

    sub = lambda x, y: [ (i - j) % 2 for i, j in zip(x, y)]

    for i in range(len(Con)):
        if Con[i][i] == 0:
            for k in range(i + 1, len(Con)):
                if Con[k][i]:
                    Con[i], Con[k] = Con[k], Con[i]
        for j in range(i + 1, len(Con)):
            if Con[j][i]:
                Con[j] = sub(Con[i], Con[j])


    for i in range(len(Con) - len(Q) - 2, 0, -1):
        for j in range(i - 1, -1, -1):
            if Con[j][i]:
                Con[j] = sub(Con[i], Con[j])


    NZ = [i for i in Con if sum(i)]
    Con = NZ + [[0 for i in range(len(Con) + 1)] for i in range(len(Con) - len(NZ))]
    sol = brute_force(NZ)
    sol = [(i // len(Q), i % len(Q))[::-1] for i, j in enumerate(sol) if j]
    return sol
from functools import reduce
from itertools import product
def blend(a, b):
    return [next(tuple([p if p == q else max(p, q) for p, q in zip(i, j)]) for i in a for j in b if all(p == q for p, q in zip(i, j) if -1 not in [p, q]))]

def brute_force(mat):
    cn = []
    for row in mat[-2:]:
        a = [[0, 1] if i else [-1] for i in row[:-1]]
        cn.append([tuple(i) for i in product(*a) if i.count(1) % 2 == row[-1]])
    if not cn:
        return []
    sols = reduce(blend, cn[::-1])
    for row in mat[:-2][::-1]:
        a = [[0, 1] if i and sols[-1][j] == -1 else [max(sols[-1][j], -1)] for j, i in enumerate(row[:-1])]
        sols = blend(sols, [tuple(i) for i in product(*a) if sum(k * l for k, l in zip(i, row) if k != -1) % 2 == row[-1]])
    return sols[0]


'''
Take the game matrix as argument
    Where 1: Light bulb is on
          0: Light bubl is off

Returns the (x, y) elements to toggle in order to solve the puzzle
'''
def solve(Q):
    C = []
    for i in range(len(Q)):
        for j in range(len(Q)):
            C.append([int(a == i or b == j) for a in range(len(Q)) for b in range(len(Q))] + [1 - Q[i][j]])

    sub = lambda x, y: [ (i - j) % 2 for i, j in zip(x, y)]
    for i in range(len(C)):
        if C[i][i] == 0:
            for k in range(i + 1, len(C)):
                if C[k][i]:
                    C[i], C[k] = C[k], C[i]
        for j in range(i + 1, len(C)):
            if C[j][i]:
                C[j] = sub(C[i], C[j])

    for i in range(len(C) - len(Q) - 2, 0, -1):
        for j in range(i - 1, -1, -1):
            if C[j][i]:
                C[j] = sub(C[i], C[j])

    NZ = [i for i in C if sum(i)]
    return [(i // len(Q), i % len(Q))[::-1] for i, j in enumerat
